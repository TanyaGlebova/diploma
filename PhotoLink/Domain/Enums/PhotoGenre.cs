﻿namespace Domain.Enums
{
    public enum PhotoGenre
    {
        Wedding,
        Event,
        Portrait,
        Product,
        FineArt,
        Fashion,
        Architectural,
        Travel,
        Photojournalism,
        Pet,
        Sports,
        Aerial,
        Scientific,
        Stock
    }
}
