﻿namespace Domain.Enums
{
    public enum MakeupType
    {
        Morning,
        Evening,
        SmokeyEyes,
        Ethnic,
        Lifting,
        Wedding,
        Retro,
        Vamp,
        Airbrushed
    }
}
