﻿namespace Domain.Enums
{
    public enum Role
    {
        Photographer = 1,
        Visagiste,
        Model
    }
}
