﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Bridges
{
    public class PhotoUser
    {
        [Column("photo_user_id")]
        public int Id { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [Column("photo_id")]
        public int PhotoId { get; set; }

        public virtual User User { get; set; }
        public virtual Photo Photo { get; set; }
    }
}
