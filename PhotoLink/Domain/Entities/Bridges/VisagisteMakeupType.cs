﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Bridges
{
    public class VisagisteMakeupType
    {
        [Column("visagiste_makeup_type_id")]
        public int Id { get; set; }
        [Column("visagiste_id")]
        public Guid VisagisteId { get; set; }
        [Column("makeup_type_id")]
        public byte MakeupTypeId { get; set; }

        public virtual Visagiste Visagiste { get; set; }
        public virtual  MakeupType MakeupType { get; set; }
    }
}
