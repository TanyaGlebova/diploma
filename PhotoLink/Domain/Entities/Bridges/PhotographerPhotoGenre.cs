﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.Bridges
{
    public class PhotographerPhotoGenre
    {
        [Column("photographer_photo_genre_id")]
        public int Id { get; set; }
        [Column("photographer_id")]
        public Guid PhotographerId { get; set; }
        [Column("photo_genre_id")]
        public byte PhotoGenreId { get; set; }

        public virtual Photographer Photographer { get; set; }
        public virtual PhotoGenre PhotoGenre { get; set; }
    }
}
