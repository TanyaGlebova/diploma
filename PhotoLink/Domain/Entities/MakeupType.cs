﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class MakeupType
    {
        [Column("makeup_type_id")]
        public byte Id { get; set; }
        [Column("name")]
        public string Name { get; set; }

        public virtual ICollection<VisagisteMakeupType> Visagistes { get; set; }
    }
}
