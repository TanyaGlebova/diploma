﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Model
    {
        [Column("model_id")]
        public Guid Id { get; set; }
        [Column("height")]
        public int Height { get; set; }
        [Column("weight")]
        public int Weight { get; set; }
    }
}
