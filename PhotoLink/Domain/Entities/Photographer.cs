﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class Photographer
    {
        [Column("photographer_id")]
        public Guid Id { get; set; }

        public virtual ICollection<PhotographerPhotoGenre> PhotoGenres { get; set; }
    }
}
