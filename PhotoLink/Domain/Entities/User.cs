﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Common;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class User : AuditableEntity
    {
        [Column("user_id")]
        public Guid Id { get; set; }
        [Column("role_id")]
        public byte? RoleId { get; set; }
        [Column("user_name")]
        public string UserName { get; set; }
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("salt")]
        public string Salt { get; set; }
        [Column("about")]
        public string About { get; set; }
        [Column("birthdate")]
        public DateTime? BirthDate { get; set; }
        [Column("education")]
        public string Education { get; set; }
        [Column("experience")]
        public DateTime? Experience { get; set; }
        [Column("location")]
        public string Location { get; set; }
        [Column("links")]
        public string Links { get; set; }

        public virtual ICollection<UserContact> UserContacts { get; set; }
        public virtual ICollection<UserContact> ContactsUser { get; set; }
        public virtual ICollection<PhotoUser> PhotoUsers { get; set; }
    }
}
