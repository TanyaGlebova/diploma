﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class UserContact
    {
        [Column("user_contact_id")]
        public int Id { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [Column("contact_id")]
        public Guid ContactId { get; set; }

        public virtual User User { get; set; }
        public virtual User Contact { get; set; }
    }
}
