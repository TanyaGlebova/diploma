﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class PhotoGenre
    {
        [Column("photo_genre_id")]
        public byte Id { get; set; }
        [Column("name")]
        public string Name { get; set; }

        public virtual ICollection<PhotographerPhotoGenre> Photographers { get; set; }
    }
}
