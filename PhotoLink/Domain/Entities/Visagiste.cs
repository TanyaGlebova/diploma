﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class Visagiste
    {
        [Column("visagiste_id")]
        public Guid Id { get; set; }

        public virtual ICollection<VisagisteMakeupType> MakeupTypes { get; set; }
    }
}
