﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain.Common;

namespace Domain.Entities
{
    public class Role : AuditableEntity
    {
        [Column("role_id")]
        public byte Id { get; set; }
        [Column("role_name")]
        public string Name { get; set; }
    }
}
