﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Bridges;

namespace Domain.Entities
{
    public class Photo
    {
        [Column("photo_id")]
        public int Id { get; set; }
        [Column("views")]
        public int Views { get; set; }
        [Column("likes")]
        public int Likes { get; set; }
        [Column("location")]
        public string Location { get; set; }
        [Column("publication_date")]
        public DateTime PublicationDate { get; set; }
        [Column("camera")]
        public string Camera { get; set; }
        [Column("focal_length")]
        public int FocalLength { get; set; }
        [Column("aperture")]
        public decimal Aperture { get; set; }
        [Column("shutter_speed")]
        public int ShutterSpeep { get; set; }
        [Column("iso")]
        public int ISO { get; set; }

        public virtual ICollection<PhotoUser> PhotoUsers { get; set; }
    }
}
