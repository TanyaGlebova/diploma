﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IList<Photo> Photos { get; set; }
    }
}
