﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users.Commands.UpdateUserRole
{
    public class UpdateUserRoleCommand : IRequest
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }

    public class UpdateUserRoleCommandHandler : IRequestHandler<UpdateUserRoleCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateUserRoleCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateUserRoleCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => u.UserName == request.UserName);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserName);
            }

            var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == request.RoleName);

            if (role == null)
            {
                throw new NotFoundException(nameof(Role), request.RoleName);
            }

            user.RoleId = role.Id;

            switch (role.Name)
            {
                case "Photographer":
                    _context.Photographers.Add(new Photographer() {Id = user.Id});
                    break;
                case "Visagiste":
                    _context.Visagistes.Add(new Visagiste() {Id = user.Id});
                    break;
                case "Model":
                    _context.Models.Add(new Model() {Id = user.Id});
                    break;
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
