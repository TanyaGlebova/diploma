﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using MediatR;

namespace Application.Users.Commands.DeleteUserContact
{
    public class DeleteUserContactCommand : IRequest
    {
        public string UserName { get; set; }
        public string ContactName { get; set; }
    }

    public class DeleteUserContactCommandHandler : IRequestHandler<DeleteUserContactCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteUserContactCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteUserContactCommand request, CancellationToken cancellationToken)
        {
            var userContact = _context.UserContacts
                .FirstOrDefault(uc => uc.User.UserName == request.UserName &&
                             uc.Contact.UserName == request.ContactName);

            if (userContact == null)
            {
                throw new NotFoundException("User contact does not exist");
            }

            _context.UserContacts.Remove(userContact);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
