﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Users.Commands.CreateUserContact
{
    public class CreateUserContactCommand : IRequest<int>
    {
        public string UserName { get; set; }
        public string ContactName { get; set; }
    }

    public class CreateUserContactCommandHandler : IRequestHandler<CreateUserContactCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateUserContactCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateUserContactCommand request, CancellationToken cancellationToken)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserName == request.UserName);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserName);
            }

            var contact = _context.Users.FirstOrDefault(u => u.UserName == request.ContactName);

            if (contact == null)
            {
                throw new NotFoundException(nameof(User), request.ContactName);
            }

            var oldUserContacts = _context.UserContacts.Where(uc => uc.UserId == user.Id);

            if (oldUserContacts.Any(olduk => olduk.ContactId == contact.Id))
            {
                throw new Exception("User can't add a contact twice");
            }

            var userContact = new UserContact()
            {
                UserId = user.Id,
                User = user,
                ContactId = contact.Id,
                Contact = contact
            };

            _context.UserContacts.Add(userContact);

            await _context.SaveChangesAsync(cancellationToken);

            return userContact.Id;
        }
    }
}
