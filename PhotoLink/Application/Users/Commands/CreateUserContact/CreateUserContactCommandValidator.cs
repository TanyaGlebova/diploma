﻿using FluentValidation;

namespace Application.Users.Commands.CreateUserContact
{
    public class CreateUserContactCommandValidator : AbstractValidator<CreateUserContactCommand>
    {
        public CreateUserContactCommandValidator()
        {
            RuleFor(v => v.ContactName).NotEqual(v => v.UserName);
        }
    }
}
