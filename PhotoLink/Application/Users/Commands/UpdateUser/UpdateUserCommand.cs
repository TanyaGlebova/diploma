﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Entities.Bridges;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users.Commands.UpdateUser
{
    public class UpdateUserCommand : IRequest
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string About { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Education { get; set; }
        public DateTime? Experience { get; set; }
        public string Location { get; set; }
        public string Links { get; set; }
        public string RoleName { get; set; }
        public List<string> PhotoGenres { get; set; }
        public List<string> MakeupTypes { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateUserCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => u.UserName == request.UserName);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserName);
            }

            user.FirstName = request.FirstName ?? user.FirstName;
            user.LastName = request.LastName ?? user.LastName;
            user.About = request.About ?? user.About;
            user.BirthDate = request.BirthDate ?? user.BirthDate;
            user.Education = request.Education ?? user.Education;
            user.Experience = request.Experience ?? user.Experience;
            user.Location = request.Location ?? user.Location;
            user.Links = request.Links ?? user.Links;

            var role = _context.Roles.FirstOrDefault(r => r.Name == request.RoleName);

            if (role == null)
            {
                throw new NotFoundException(nameof(Role), request.RoleName);
            }

            switch (role.Name)
            {
                case "Photographer":
                    var photographer = await _context.Photographers.FindAsync(user.Id);
                    await UpdatePhotographerPhotoGenres(photographer, request.PhotoGenres);
                    break;
                case "Visagiste":
                    var visagiste = await _context.Visagistes.FindAsync(user.Id);
                    await UpdateVisagisteMakeupTypes(visagiste, request.MakeupTypes);
                    break;
                case "Model":
                    var model = await _context.Models.FindAsync(user.Id);
                    model.Height = request.Height;
                    model.Weight = request.Weight;
                    break;
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private async Task UpdatePhotographerPhotoGenres(Photographer photographer, List<string> photoGenreNames)
        {
            var photoGenres = await _context.PhotoGenres
                .Where(pg => photoGenreNames.Contains(pg.Name))
                .ToListAsync();

            var newPhotographerPhotoGenres = photoGenres.Select(phg =>
                new PhotographerPhotoGenre()
                {
                    PhotographerId = photographer.Id,
                    Photographer = photographer,
                    PhotoGenreId = phg.Id,
                    PhotoGenre = phg
                });

            var oldPhotographerPhotoGenres = _context.PhotographerPhotoGenres.Where(ppg => ppg.PhotographerId == photographer.Id);

            foreach (var newppg in newPhotographerPhotoGenres)
            {
                if (!oldPhotographerPhotoGenres.Any(ppg => ppg.PhotoGenre == newppg.PhotoGenre))
                    _context.PhotographerPhotoGenres.Add(newppg);
            }

            foreach (var oldppg in oldPhotographerPhotoGenres)
            {
                if (!newPhotographerPhotoGenres.Any(ppg => ppg.PhotoGenre == oldppg.PhotoGenre))
                    _context.PhotographerPhotoGenres.Remove(oldppg);
            }
        }

        private async Task UpdateVisagisteMakeupTypes(Visagiste visagiste, List<string> makeupTypesNames)
        {
            var makeupTypes = await _context.MakeupTypes
                .Where(mt => makeupTypesNames.Contains(mt.Name))
                .ToListAsync();

            var newVisagisteMakeupTypes = makeupTypes.Select(mt =>
                new VisagisteMakeupType()
                {
                    VisagisteId = visagiste.Id,
                    Visagiste = visagiste,
                    MakeupTypeId = mt.Id,
                    MakeupType = mt
                });

            var oldVisagisteMakeupTypes = _context.VisagisteMakeupTypes.Where(vmt => vmt.VisagisteId == visagiste.Id);

            foreach (var newvmt in newVisagisteMakeupTypes)
            {
                if (!oldVisagisteMakeupTypes.Any(vmt => vmt.MakeupType == newvmt.MakeupType))
                    _context.VisagisteMakeupTypes.Add(newvmt);
            }

            foreach (var oldvmt in oldVisagisteMakeupTypes)
            {
                if (!newVisagisteMakeupTypes.Any(vmt => vmt.MakeupType == oldvmt.MakeupType))
                    _context.VisagisteMakeupTypes.Remove(oldvmt);
            }
        }
    }
}
