﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Application.Users.Queries.GetUser
{
    public class UserVm
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string About { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Education { get; set; }
        public DateTime? Experience { get; set; }
        public string Location { get; set; }
        public string Links { get; set; }
        public string RoleName { get; set; }
        public List<string> PhotoGenres { get; set; }
        public List<string> MakeupTypes { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }

        public UserVm(User user)
        {
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            About = user.About;
            BirthDate = user.BirthDate;
            Education = user.Education;
            Experience = user.Experience;
            Location = user.Location;
            Links = user.Links;
        }
    }
}
