﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserVm>
    {
        public string UserName { get; set; }
    }

    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserVm>
    {
        private readonly IApplicationDbContext _context;

        public GetUserQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UserVm> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == request.UserName);
            var userVm = new UserVm(user);
            //userVm.RoleName = 
            switch (user.RoleId)
            {
                case (int)Role.Photographer:
                    var photographerPhotoGenres = _context.PhotographerPhotoGenres
                        .Where(ppg => ppg.PhotographerId == user.Id);
                    userVm.PhotoGenres = photographerPhotoGenres.Select(ppg => ppg.PhotoGenre.Name).ToList();
                    break;
                case (int)Role.Visagiste:
                    var visagisteMakeupTypes = _context.VisagisteMakeupTypes
                        .Where(vmt => vmt.VisagisteId == user.Id);
                    userVm.MakeupTypes = visagisteMakeupTypes.Select(vmt => vmt.MakeupType.Name).ToList();
                    break;
                case (int)Role.Model:
                    var model = await _context.Models.FirstOrDefaultAsync(m => m.Id == user.Id);
                    userVm.Height = model.Height;
                    userVm.Weight = model.Weight;
                    break;
            }

            return userVm;
        }
    }
}
