﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users.Queries.GetUserContacts
{
    public class GetUserContactsQuery : IRequest<UserContactsVm>
    {
        public string UserName { get; set; }
    }

    public class GetUserContactsQueryHandler : IRequestHandler<GetUserContactsQuery, UserContactsVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUserContactsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserContactsVm> Handle(GetUserContactsQuery request, CancellationToken cancellationToken)
        {
            return new UserContactsVm()
            {
                UserContacts = await _context.UserContacts
                    .Where(uc => uc.User.UserName == request.UserName)
                    .ProjectTo<UserContactDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}
