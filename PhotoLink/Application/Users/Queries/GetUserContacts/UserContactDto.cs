﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Users.Queries.GetUserContacts
{
    public class UserContactDto : IMapFrom<UserContact>
    {
        public string UserName { get; set; }
        public string ContactName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UserContact, UserContactDto>()
                .ForMember(dto => dto.UserName, opt => opt.MapFrom(uc => uc.User.UserName))
                .ForMember(dto => dto.ContactName, opt => opt.MapFrom(uc => uc.Contact.UserName));
        }
    }
}
