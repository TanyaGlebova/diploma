﻿using System.Collections.Generic;

namespace Application.Users.Queries.GetUserContacts
{
    public class UserContactsVm
    {
        public IList<UserContactDto> UserContacts { get; set; }
    }
}
