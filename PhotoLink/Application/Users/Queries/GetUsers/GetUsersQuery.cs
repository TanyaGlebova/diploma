﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<UsersVm>
    {
    }

    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, UsersVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUsersQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UsersVm> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            return new UsersVm
            {
                Users = await _context.Users
                    .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                    .OrderBy(u => u.LastName)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}
