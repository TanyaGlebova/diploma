﻿using System.Collections.Generic;

namespace Application.Users.Queries.GetUsers
{
    public class UsersVm
    {
        public IList<UserDto> Users { get; set; }
    }
}
