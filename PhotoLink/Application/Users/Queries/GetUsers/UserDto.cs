﻿using System;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Users.Queries.GetUsers
{
    public class UserDto : IMapFrom<User>
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string About { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Education { get; set; }
        public DateTime? Experience { get; set; }
        public string Location { get; set; }
        public string Links { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<User, UserDto>();
        }
    }
}
