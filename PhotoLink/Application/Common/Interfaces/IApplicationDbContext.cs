﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities.Bridges;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<Photographer> Photographers { get; set; }
        DbSet<PhotoGenre> PhotoGenres { get; set; }
        DbSet<PhotographerPhotoGenre> PhotographerPhotoGenres { get; set; }
        DbSet<Visagiste> Visagistes { get; set; }
        DbSet<MakeupType> MakeupTypes { get; set; }
        DbSet<VisagisteMakeupType> VisagisteMakeupTypes { get; set; }
        DbSet<Model> Models { get; set; }
        DbSet<UserContact> UserContacts { get; set; }
        DbSet<Domain.Entities.Photo> Photos { get; set; }
        DbSet<Domain.Entities.Bridges.PhotoUser> PhotoUsers { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
