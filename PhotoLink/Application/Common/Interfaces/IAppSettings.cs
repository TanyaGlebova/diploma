﻿namespace Application.Common.Interfaces
{
    public interface IAppSettings
    {
        string Secret { get; set; }
    }
}
