﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.PhotoUser.Commands.CreatePhotoUser
{
    public class CreatePhotoUserCommand : IRequest<int>
    {
        public string UserName { get; set; }
        public int? PhotoId { get; set; }
    }

    public class CreatePhotoUserCommandHandler : IRequestHandler<CreatePhotoUserCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreatePhotoUserCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreatePhotoUserCommand request, CancellationToken cancellationToken)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserName == request.UserName);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserName);
            }

            var photo = new Domain.Entities.Photo(){PublicationDate = DateTime.Now};

            _context.Photos.Add(photo);

            await _context.SaveChangesAsync(cancellationToken);

            var photoUser = new Domain.Entities.Bridges.PhotoUser()
            {
                PhotoId = photo.Id,
                UserId = user.Id
            };

            _context.PhotoUsers.Add(photoUser);

            await _context.SaveChangesAsync(cancellationToken);

            return photo.Id;
        }
    }
}
