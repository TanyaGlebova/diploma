﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Authentication.Helpers;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.IdentityModel.Tokens;

namespace Application.Authentication.Commands.CreateUser
{
    public class SignInCommand : IRequest<string>
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<SignInCommand, string>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAppSettings _appSettings;

        public CreateUserCommandHandler(IApplicationDbContext context, IAppSettings appSettings)
        {
            _context = context;
            _appSettings = appSettings;
        }

        public async Task<string> Handle(SignInCommand request, CancellationToken cancellationToken)
        {
            var salt = Salt.Create();
            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = request.UserName,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Salt = salt,
                Password = Hash.Create(request.Password, salt)
            };

            _context.Users.Add(user);

            await _context.SaveChangesAsync(cancellationToken);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
