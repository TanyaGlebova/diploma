﻿using Application.Authentication.Commands.CreateUser;
using FluentValidation;

namespace Application.Authentication.Commands
{
    public class SignInCommandValidator : AbstractValidator<SignInCommand>
    {
        public SignInCommandValidator()
        {
            RuleFor(v => v.UserName).MaximumLength(25).NotEmpty();
            RuleFor(v => v.FirstName).MaximumLength(25).NotEmpty();
            RuleFor(v => v.LastName).MaximumLength(25).NotEmpty();
            RuleFor(v => v.Email).Matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
            RuleFor(v => v.Password).Length(8, 25);
        }
    }
}
