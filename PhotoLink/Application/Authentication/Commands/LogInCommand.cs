﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Authentication.Helpers;
using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Application.Authentication.Commands
{
    public class LogInCommand : IRequest<string>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class LogInCommandHandler : IRequestHandler<LogInCommand, string>
    {
        private readonly IApplicationDbContext _context;
        private readonly IAppSettings _appSettings;

        public LogInCommandHandler(IApplicationDbContext context, IAppSettings appSettings)
        {
            _context = context;
            _appSettings = appSettings;
        }

        public async Task<string> Handle(LogInCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.Where(u => u.Email == request.Email)
                .FirstOrDefaultAsync();

            var isPasswordValid = Hash.Validate(request.Password, user.Salt, user.Password);

            if (user == null || !isPasswordValid)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
