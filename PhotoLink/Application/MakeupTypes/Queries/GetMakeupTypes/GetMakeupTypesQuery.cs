﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.MakeupTypes.Queries.GetMakeupTypes
{
    public class GetMakeupTypesQuery : IRequest<MakeupTypesVm>
    {
    }

    public class GetMakeupTypesQueryHandler : IRequestHandler<GetMakeupTypesQuery, MakeupTypesVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetMakeupTypesQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<MakeupTypesVm> Handle(GetMakeupTypesQuery request, CancellationToken cancellationToken)
        {
            return new MakeupTypesVm()
            {
                MakeupTypes = await _context.MakeupTypes
                    .ProjectTo<MakeupTypeDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken)
            };
        }
    }


}
