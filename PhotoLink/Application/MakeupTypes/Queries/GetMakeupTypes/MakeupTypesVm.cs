﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MakeupTypes.Queries.GetMakeupTypes
{
    public class MakeupTypesVm
    {
        public List<MakeupTypeDto> MakeupTypes { get; set; }
    }
}
