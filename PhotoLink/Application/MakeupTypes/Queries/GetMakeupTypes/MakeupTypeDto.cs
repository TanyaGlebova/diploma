﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.MakeupTypes.Queries.GetMakeupTypes
{
    public class MakeupTypeDto : IMapFrom<MakeupType>
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
