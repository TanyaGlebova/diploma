﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Photo.Commands.UpdatePhoto
{
    public class UpdatePhotoCommand : IRequest
    {
        public string UserName { get; set; }
        public int PhotoId { get; set; }
        public int? Views { get; set; }
        public int? Likes { get; set; }
        public string Location { get; set; }
        public string Camera { get; set; }
        public int? FocalLength { get; set; }
        public decimal? Aperture { get; set; }
        public int? ShutterSpeed { get; set; }
        public int? Iso { get; set; }
    }

    public class UpdatePhotoCommandHandler : IRequestHandler<UpdatePhotoCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdatePhotoCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdatePhotoCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => u.UserName == request.UserName);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserName);
            }

            if (!_context.PhotoUsers.Any(phu =>
                phu.PhotoId == request.PhotoId && phu.User.UserName == request.UserName))
            {
                throw new NotFoundException("User is not allowed to update info for this photo");
            }

            var photo = await _context.Photos.FindAsync(request.PhotoId);

            photo.Views = request.Views ?? photo.Views;
            photo.Likes = request.Likes ?? photo.Likes;
            photo.Location = request.Location ?? photo.Location;
            photo.Camera = request.Camera ?? photo.Camera;
            photo.FocalLength = request.FocalLength ?? photo.FocalLength;
            photo.Aperture = request.Aperture ?? photo.Aperture;
            photo.ShutterSpeep = request.ShutterSpeed ?? photo.ShutterSpeep;
            photo.ISO = request.Iso ?? photo.ISO;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
