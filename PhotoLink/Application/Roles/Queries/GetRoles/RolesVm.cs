﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Application.Roles.Queries.GetRoles
{
    public class RolesVm
    {
        public List<Role> Roles { get; set; }
    }
}
