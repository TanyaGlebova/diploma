﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Roles.Queries.GetRoles
{
    public class GetRolesQuery : IRequest<RolesVm>
    {
    }

    public class GetRolesQueryHandler : IRequestHandler<GetRolesQuery, RolesVm>
    {
        private readonly IApplicationDbContext _context;

        public GetRolesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RolesVm> Handle(GetRolesQuery request, CancellationToken cancellationToken)
        {
            return new RolesVm()
            {
                Roles = await _context.Roles.ToListAsync(cancellationToken)
            };
        }
    }
}
