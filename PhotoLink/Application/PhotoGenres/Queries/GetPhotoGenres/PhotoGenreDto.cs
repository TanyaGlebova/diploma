﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.PhotoGenres.Queries.GetPhotoGenres
{
    public class PhotoGenreDto : IMapFrom<PhotoGenre>
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
