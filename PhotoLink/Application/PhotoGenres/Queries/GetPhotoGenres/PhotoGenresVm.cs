﻿using System.Collections.Generic;

namespace Application.PhotoGenres.Queries.GetPhotoGenres
{
    public class PhotoGenresVm
    {
        public List<PhotoGenreDto> PhotoGenres { get; set; }
    }
}
