﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.PhotoGenres.Queries.GetPhotoGenres
{
    public class GetPhotoGenresQuery : IRequest<PhotoGenresVm>
    {
    }

    public class GetPhotoGenresQueryHandler : IRequestHandler<GetPhotoGenresQuery, PhotoGenresVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPhotoGenresQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PhotoGenresVm> Handle(GetPhotoGenresQuery request, CancellationToken cancellationToken)
        {
            return new PhotoGenresVm()
            {
                PhotoGenres = await _context.PhotoGenres
                    .ProjectTo<PhotoGenreDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}
