﻿using System.Threading.Tasks;
using Application.PhotoUser.Commands.CreatePhotoUser;
using Application.Users.Commands.CreateUserContact;
using Application.Users.Commands.DeleteUserContact;
using Application.Users.Commands.UpdateUser;
using Application.Users.Commands.UpdateUserRole;
using Application.Users.Queries.GetUser;
using Application.Users.Queries.GetUserContacts;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    [Route("user")]
    public class UserController : ApiController
    {
        [HttpGet("{userName}")]
        public async Task<ActionResult<UserVm>> Get(string userName)
        {
            return await Mediator.Send(new GetUserQuery(){UserName = userName});
        }

        [HttpPut("{userName}")]
        public async Task<ActionResult> Update(string userName, UpdateUserCommand command)
        {
            if (userName != command.UserName)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpGet("{userName}/contact")]
        public async Task<ActionResult<UserContactsVm>> GetContacts(string userName)
        {
            return await Mediator.Send(new GetUserContactsQuery() {UserName = userName});
        }

        [HttpPost("{userName}/contact")]
        public async Task<ActionResult<CreateUserContactCommand>> CreateUserContact(
            string userName,
            CreateUserContactCommand command)
        {
            await Mediator.Send(command);

            return command;
        }

        [HttpDelete("{userName}/contact/{contactName}")]
        public async Task<ActionResult> Delete(string userName, string contactName)
        {
            await Mediator.Send(new DeleteUserContactCommand() {UserName = userName, ContactName = contactName});

            return NoContent();
        }

        [HttpPut("{userName}/role")]
        public async Task<ActionResult> AddRole(string userName, UpdateUserRoleCommand command)
        {
            if (userName != command.UserName)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost("{userName}/photo")]
        public async Task<ActionResult<CreatePhotoUserCommand>> CreateUserPhoto(
            string userName,
            CreatePhotoUserCommand command)
        {
            command.PhotoId = await Mediator.Send(command);
            return command;
        }
    }
}
