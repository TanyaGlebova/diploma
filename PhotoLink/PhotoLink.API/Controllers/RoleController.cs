﻿using System.Threading.Tasks;
using Application.Roles.Queries.GetRoles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    //[Authorize]
    [Route("role")]
    public class RoleController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<RolesVm>> Get()
        {
            return await Mediator.Send(new GetRolesQuery());
        }
    }
}
