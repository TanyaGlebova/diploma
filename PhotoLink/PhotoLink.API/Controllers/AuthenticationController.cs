﻿using System.Threading.Tasks;
using Application.Authentication.Commands;
using Application.Authentication.Commands.CreateUser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    [Route("authentication")]
    public class AuthenticationController : ApiController
    {
        [AllowAnonymous]
        [HttpPost("signin")]
        public async Task<ActionResult> SignIn(SignInCommand command)
        {
            var token = await Mediator.Send(command);

            return Ok(token);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult> Login(LogInCommand command)
        {
            var token = await Mediator.Send(command);

            if (token == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            return Ok(token);
        }
    }
}
