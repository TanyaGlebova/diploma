﻿using System.Threading.Tasks;
using Application.PhotoGenres.Queries.GetPhotoGenres;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    //[Authorize]
    [Route("photoGenre")]
    public class PhotoGenreController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<PhotoGenresVm>> Get()
        {
            return await Mediator.Send(new GetPhotoGenresQuery());
        }
    }
}
