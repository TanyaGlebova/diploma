﻿using System.Threading.Tasks;
using Application.MakeupTypes.Queries.GetMakeupTypes;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    //[Authorize]
    [Route("makeupType")]
    public class MakeupTypeController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<MakeupTypesVm>> Get()
        {
            return await Mediator.Send(new GetMakeupTypesQuery());
        }
    }
}
