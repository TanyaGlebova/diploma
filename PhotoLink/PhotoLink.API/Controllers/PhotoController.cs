﻿using System.Threading.Tasks;
using Application.Photo.Commands.UpdatePhoto;
using Microsoft.AspNetCore.Mvc;

namespace PhotoLink.API.Controllers
{
    [Route("photo")]
    public class PhotoController : ApiController
    {
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdatePhotoCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }
    }
}
