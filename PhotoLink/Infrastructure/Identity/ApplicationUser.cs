﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Identity
{
    [Table("AspNetUsers")]
    public class ApplicationUser : IdentityUser
    {
    }
}
