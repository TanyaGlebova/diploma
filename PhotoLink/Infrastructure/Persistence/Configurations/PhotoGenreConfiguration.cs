﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class PhotoGenreConfiguration : IEntityTypeConfiguration<PhotoGenre>
    {
        public void Configure(EntityTypeBuilder<PhotoGenre> builder)
        {
            builder.ToTable("photo_genre");

            builder.Property(pg => pg.Name).HasMaxLength(25).IsRequired();
        }
    }
}
