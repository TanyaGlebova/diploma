﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("user");

            builder.Property(u => u.UserName).HasMaxLength(25).IsRequired();
            builder.Property(u => u.FirstName).HasMaxLength(25).IsRequired();
            builder.Property(u => u.LastName).HasMaxLength(25).IsRequired();
            builder.Property(u => u.Email).HasMaxLength(50).IsRequired();
            builder.Property(u => u.Password).HasMaxLength(128).IsRequired();
            builder.Property(u => u.Salt).HasMaxLength(128).IsRequired();
            builder.Property(u => u.About).HasMaxLength(250);
            builder.Property(u => u.Education).HasMaxLength(250);
            builder.Property(u => u.Location).HasMaxLength(128);
            builder.Property(u => u.Links).HasMaxLength(250);
        }
    }
}
