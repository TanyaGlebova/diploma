﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class MakeupTypeConfiguration : IEntityTypeConfiguration<MakeupType>
    {
        public void Configure(EntityTypeBuilder<MakeupType> builder)
        {
            builder.ToTable("makeup_type");

            builder.Property(mt => mt.Name).HasMaxLength(25).IsRequired();
        }
    }
}
