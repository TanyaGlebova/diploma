﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class PhotoConfiguration : IEntityTypeConfiguration<Photo>
    {
        public void Configure(EntityTypeBuilder<Photo> builder)
        {
            builder.ToTable("photo");

            builder.Property(ph => ph.Location).HasMaxLength(128);
            builder.Property(ph => ph.PublicationDate).IsRequired();
            builder.Property(ph => ph.Camera).HasMaxLength(50);
        }
    }
}
