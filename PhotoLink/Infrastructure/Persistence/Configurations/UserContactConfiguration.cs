﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class UserContactConfiguration : IEntityTypeConfiguration<UserContact>
    {
        public void Configure(EntityTypeBuilder<UserContact> builder)
        {
            builder.ToTable("user_contact");

            builder.HasOne(uc => uc.User)
                .WithMany(user => user.UserContacts)
                .HasForeignKey(uc => uc.UserId);

            builder.HasOne(uc => uc.Contact)
                .WithMany(user => user.ContactsUser)
                .HasForeignKey(uc => uc.ContactId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
