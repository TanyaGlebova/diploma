﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class VisagisteConfiguration : IEntityTypeConfiguration<Visagiste>
    {
        public void Configure(EntityTypeBuilder<Visagiste> builder)
        {
            builder.ToTable("visagiste");
        }
    }
}
