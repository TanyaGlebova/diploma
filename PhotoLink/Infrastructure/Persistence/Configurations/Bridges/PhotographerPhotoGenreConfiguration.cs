﻿using Domain.Entities.Bridges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations.Bridges
{
    public class PhotographerPhotoGenreConfiguration : IEntityTypeConfiguration<PhotographerPhotoGenre>
    {
        public void Configure(EntityTypeBuilder<PhotographerPhotoGenre> builder)
        {
            builder.ToTable("photographer_photo_genre");

            builder.HasOne(ppg => ppg.Photographer)
                .WithMany(photographer => photographer.PhotoGenres)
                .HasForeignKey(ppg => ppg.PhotographerId);

            builder.HasOne(ppg => ppg.PhotoGenre)
                .WithMany(photogenre => photogenre.Photographers)
                .HasForeignKey(ppg => ppg.PhotoGenreId);
        }
    }
}
