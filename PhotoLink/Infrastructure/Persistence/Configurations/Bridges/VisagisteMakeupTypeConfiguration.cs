﻿using Domain.Entities.Bridges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations.Bridges
{
    public class VisagisteMakeupTypeConfiguration : IEntityTypeConfiguration<VisagisteMakeupType>
    {
        public void Configure(EntityTypeBuilder<VisagisteMakeupType> builder)
        {
            builder.ToTable("visagiste_makeup_type");

            builder.HasOne(vmt => vmt.Visagiste)
                .WithMany(visagiste => visagiste.MakeupTypes)
                .HasForeignKey(vmt => vmt.VisagisteId);

            builder.HasOne(vmt => vmt.MakeupType)
                .WithMany(makeuptype => makeuptype.Visagistes)
                .HasForeignKey(vmt => vmt.MakeupTypeId);

        }
    }
}
