﻿using Domain.Entities.Bridges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations.Bridges
{
    public class PhotoUserConfiguration : IEntityTypeConfiguration<PhotoUser>
    {
        public void Configure(EntityTypeBuilder<PhotoUser> builder)
        {
            builder.ToTable("photo_user");

            builder.HasOne(pu => pu.User)
                .WithMany(user => user.PhotoUsers)
                .HasForeignKey(pu => pu.UserId);

            builder.HasOne(pu => pu.Photo)
                .WithMany(photo => photo.PhotoUsers)
                .HasForeignKey(pu => pu.PhotoId);
        }
    }
}
