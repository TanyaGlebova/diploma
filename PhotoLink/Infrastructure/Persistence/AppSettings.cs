﻿using Application.Common.Interfaces;

namespace Infrastructure.Persistence
{
    public class AppSettings : IAppSettings
    {
        public string Secret { get; set; }
    }
}
