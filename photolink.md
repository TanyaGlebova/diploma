# The PhotoLink project

## Концепция

Веб-приложение, разрабатываемое в рамках дипломного проекта, нацелено на предоставление комфортного способа коммуникации и коллаборации между фотографами, моделями и визажистами.
Решает задачу обособления данного процесса от платформ (в частности, социальных сетей), специально для этого не предназначенных, а также предоставляет специализированный функционал,
необходимый данным категориям пользователей.

***

## Обзор функциональности

Сущности проекта:

* **Пользователь.** Содержит в себе
    * Роль. Добавляется под фото, участником которого является пользователь, а также подразумевает дополнительные специальные поля в разделе информации о пользователе: 
      для модели - рост, вес; для фотографа - жанр фотографий; для визажиста - вид макияжа;
    * Фамилия и Имя пользователя. Отображаются на странице пользователя
    * Никнейм. Указывается при регистрации. Идея в том, чтобы использовать его в URL профиля
    * Текстовая информация о себе (или любой другой текст), будет отображаться в профиле под аватаром
    * Дата рождения. В профиле планируется отображать только возраст пользователя
    * Профессиональное образование (курсы, школы и т.д.)
    * Опыт работы и достижения (текстовое поле)
    * Местоположение (страна, город)
    * Ссылки на соцсети (Instagram, Facebook)
    
* **Фото.** Содержит в себе:
    * Участников фотографии (роль, имя пользователя, фото профиля)
    * Количество просмотров
    * Рейтинг. На сегодняшний день Instagram отказывается от лайков, во всяком случае, решается вопрос отображения их количества под фото. В PhotoLink будет предусмотрена
      возможность поставить лайк на фото, который будет влиять на его рейтинг. Рейтинг = кол-во лайков / общее кол-во просмотров фото. Таким образом,
      владелец профиля может гнаться не за цифрой, а за качеством контента своего профиля.
    * Локация съемки. Устанавливается пользователем, который загружает фото.
    * Дата. Автоматически выставляется равной дате загрузки
    * Инфорамия о фотоаппарате
        * Модель
        * Фокусное расстояние
        * Диафрагма
        * Выдержка
        * Светочувствительность ISO
    * Теги
    * Комментарии
        * Пользователь (имя пользователя, фото профиля)
        * Текст комментария
        * Дата
    * Само фото
    
* **Проект.** Состоит из:
    * Участников проекта (один или более фотограф, визажист и модель)
    * Фотографий в проекте (со всей информацией, соответствующей сущности фото). При этом фотограф, визажист и модель для конкретной фотографии входят в общий список участников проекта.

Разделение на личные фото владельца профиля и фото-коллаборации и проекты:

* Пользователь с любой ролью может загружать изображения в свой профиль, и они появятся в разделе Photos на его странице
* В случае, когда первый пользователь отмечает второго как участника на отдельной фотографии (не входящей в проект) или добавляет его в проект, все фото и проекты должны быть доступны у обоих пользователей в соответствующих разделах на их страницах. Разделять фото и проекты на "My photos/projects" и "All" нецелесообразно, поскольку категории пользователей Визажист и Модель, в отличие от Фотографов, будут выступать в роли создателей фото (тех, кто загрузил фото на сайт) или проектов гораздо реже. И это будет выглядеть, как: в профиле модели (или визажиста) в разделе Photos > MyPhotos почти нет фотографий, поскольку в конечном итоге скорее Фотограф будет тем человеком, который загрузит общее для них фото на сайт.
      
***

В общую для всех страниц панель навигации входят:

* В левой части:
    * ссылка с названием приложения для перехода на главную страницу
* В правой части:
    * поисковая кнопка (для поиска отводится отдельная страница т.к. предполагается поиск как пользователей так и фотографий и проектов)
    * кнопка создания нового проекта. Здесь можно было бы добавить возможность не только добавить участника, 
      но и оставить заявку на участника по определенным критериям внутри проекта
    * кнопка для перехода к профилю пользователя
    * переход к диалогам
    * раздел статистики. В mvp версии планируется только количественная статистика: общее число лайков, контактов за всё время и т.д. В зависимости от 
      времени процесса разработки в раздел можно будет добавить статистику на основе графиков зависимостей.
    * кнопка для загрузки изображений.

***

Приложение делится на несколько логических "модулей":

1. **Домашняя страница.** Содержит основную информацию о приложении: для чего, для кого, основная идея, а также ленту новостей из последних публикаций пользователей

    * > ![](https://bitbucket.org/TanyaGlebova/diploma/raw/fc5f02c10a75552f364292e98556e1f8eef4504b/images/mainpage.png)

2. **Профиль пользователя.** Вид страницы будет отличаться для собственного профиля и для профиля другого пользователя: в первом случае во вкладке About будет доступно 
   редактирование информации о себе, во втором будут добавлены кнопки Contact и Message. В приложении было решено отказаться от подписок и подписчиков, заменив их 
   сетью контаков. Добавление контакта представляет собой следующее действие: один пользователь жмет кнопку "Contact" со страницы с фото или со страницы второго пользователя, чтобы добавить его к своим контактам. При этом второй пользователь не уведомляется об этом, а первый пользователь может в любое время написать второму или просмотреть его профиль, перейдя на его страницу из вкладки Contacts своей страницы. На странице обязательно отображается имя пользователя, остальная информация в зависимости от того, указана ли она.

    * > ![](https://bitbucket.org/TanyaGlebova/diploma/raw/736f4ef2100cc0269daaa40e181ace1f09bb1055/images/userpage.png) 

3. **Статистика.** Основная идея в возможности отслеживания своего профессионального прогресса и качества контента. В MVP версию войдет только количественная статистика, 
   графики по мере успеваемости.
   Отображаемые значения статистики:
    * Your contacts - кол-во ваших контактов
    * You're a contact for - кол-во добавлений вас в число контактов других пользователей
    * Your projects - кол-во созданных вами проектов
    * You're in projects - кол-во проектов, в которых вы поучаствовали
    * Аналогичная двум пунткам выше статистика для фотографий, не входящих в проекты
    * Photo views - общее количество просмотров фото пользователя (как отдельных, так и внутри проектов)
    * В MVP версии добавится значение рейтинга профиля, не зависящее от временных промежутков: сумма рейтингов всех фото профиля / кол-во фото профиля. Это значение 
      также планируется использовать для поиска профиля пользователя. 

    Дизайн страницы находится не в итоговом состоянии, возможны изменения кол-ва типов значений статистики

    * > ![](https://bitbucket.org/TanyaGlebova/diploma/raw/92f4bab15b10149493aafc7580746843c0b41b0b/images/statisticspage.png)

4. **Поиск.** Предполагается самой комплексной частью приложения, поскольку будет включать в себя поиск по пользователям, фото и проектам (в перспективе). Критерии для поиска:
    * Пользователя:
        * Роль
        * Имя и (или) фамилия
        * Возраст (от - до)
        * Опыт (от - до, в годах)
        * Рейтинг (имеется в виду рейтинг профиля, описанный в разделе статистики)
        * Локация
        * Дополнительные критерии, зависящие от роли пользователя
            * Для фотографа - жанр фото
            * Для модели - рост и вес
            * Для визажиста - вид макияжа
    * Фотографии:
        * Рейтинг (от - до)
        * Дата публикации (от - до)
        * Теги
        * Модель и настройки фотоаппарата
        * Локация
    * Проекта: могут быть схожи с критериями для фото, но при наличии возможности оставить заявку внутри проекта можно еще организовать поиск по активным заявкам в проектах,
      таким образом обеспечивалась бы возможность поучаствовать в каком-либо проекте для любых специалистов, если у них, скажем, нет запланированных коллабораций в ближайшее время

    * > ![](https://bitbucket.org/TanyaGlebova/diploma/raw/736f4ef2100cc0269daaa40e181ace1f09bb1055/images/searchpage.png)  

5. **Просмотр фотографии.** Такой вид предполагается для просмотра любого фото вне зависимости от того, откуда оно открыто: со страницы профиля или из новостной ленты и т.д. 
   Будет отображаться в принципе вся информация, описанная выше в сущности фото. Обязательно у фото должен быть хотя бы один участник - фотограф, т.к. это может быть работа,
   сделанная до регистрации в приложении или среди пользователей может не быть других участников, чтобы их можно было обозначить или это может быть предметное фото. Также 
   обязательно будут присутсвовать: кол-во просмотров, рейтинг, раздел для комментариев и дата публикации. Остальная информация в зависимости от того, указали ее или нет.

    * > ![](https://bitbucket.org/TanyaGlebova/diploma/raw/736f4ef2100cc0269daaa40e181ace1f09bb1055/images/photopage.png)
    
## База данных

Схема базы данных представлена на диаграмме ниже:

> ![](https://bitbucket.org/TanyaGlebova/diploma/raw/92f4bab15b10149493aafc7580746843c0b41b0b/images/PhysicalDiagram.svg)

